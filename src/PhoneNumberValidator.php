<?php

namespace RafailDuniashev;


class PhoneNumberValidator extends AbstractValidator
{
    public function validate($value, ConstrainInterface $constraint): void
    {
        if ($value === null || $value === '') {
            return;
        }

        if (!is_string($value)) {
            throw new \UnexpectedValueException('Expected a string value. ' . ucfirst(gettype($value)) . ' given.');
        }

        $pattern = '/^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/';

        if (!preg_match($pattern, $value)) {
            $this->addError($value, $constraint->getMessage());
        }
    }
}
