<?php

namespace RafailDuniashev;


abstract class AbstractValidator implements ValidatorInterface
{
    protected $errors = [];

    protected function addError($value, string $message): void
    {
        $this->errors[] = [$value => $message];
    }

    public function hasErrors(): bool
    {
        return !empty($this->errors);
    }

    public function getErrors(): array
    {
        return $this->errors;
    }

}