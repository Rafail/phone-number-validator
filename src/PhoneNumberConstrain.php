<?php

namespace RafailDuniashev;


class PhoneNumberConstrain implements ConstrainInterface
{
    public function getMessage(): string
    {
        return 'Phone number is incorrect';
    }

}
