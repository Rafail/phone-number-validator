<?php

namespace RafailDuniashev;


interface ValidatorInterface
{
    public function validate($value, ConstrainInterface $constraint): void;
}
