<?php

require __DIR__ . '/../vendor/autoload.php';

use RafailDuniashev\PhoneNumberValidator;
use RafailDuniashev\PhoneNumberConstrain;

$validator = new PhoneNumberValidator();
$validator->validate('8-925-000-15-18', new PhoneNumberConstrain());

if ($validator->hasErrors()) { // no errors
    var_dump($validator->getErrors());
}

$validator->validate('asdcasc', new PhoneNumberConstrain());

if ($validator->hasErrors()) { // there is an error
    var_dump($validator->getErrors());
}