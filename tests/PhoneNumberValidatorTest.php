<?php

use PHPUnit\Framework\TestCase;
use RafailDuniashev\PhoneNumberValidator;
use RafailDuniashev\PhoneNumberConstrain;

class PhoneNumberValidatorTest extends TestCase
{
    public function testValidatorWithCorrectValue()
    {
        $validator = new PhoneNumberValidator();
        $validator->validate('8-925-000-15-18', new PhoneNumberConstrain());
        $this->assertEmpty($validator->getErrors());
    }

    public function testValidatorWithIncorrectValue()
    {
        $validator = new PhoneNumberValidator();
        $validator->validate('sdsfsdfs', new PhoneNumberConstrain());
        $this->assertNotEmpty($validator->getErrors());
    }

    public function testValidatorWithNullValue()
    {
        $validator = new PhoneNumberValidator();
        $validator->validate(null, new PhoneNumberConstrain());
        $this->assertEmpty($validator->getErrors());
    }

    public function testValidatorWithIncorrectTypeValue()
    {
        $validator = new PhoneNumberValidator();

        try {
            $validator->validate(new PhoneNumberValidator(), new PhoneNumberConstrain());
        } catch (\Throwable $e) {
            if ($e instanceof \UnexpectedValueException) {
                $this->assertTrue(true);
                return;
            }
        }

        $this->assertTrue(false);
    }

}
